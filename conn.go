package sshutils

import (
	"code.google.com/p/go.crypto/ssh"
	"strconv"
)

type Conn struct {
	Host string
	Port int
	User string
	Pass string
	conn *ssh.ClientConn
}

func NewConn(host string, port int, username string, password string) *Conn {
	return &Conn{
		Host: host,
		Port: port,
		User: username,
		Pass: password,
	}
}

func (c *Conn) Connect() error {
	config := &ssh.ClientConfig{
		User: c.User,
		Auth: []ssh.ClientAuth{
			ssh.ClientAuthPassword(c),
		},
	}
	host := c.Host + ":" + strconv.Itoa(c.Port)
	var err error
	c.conn, err = ssh.Dial("tcp", host, config)
	return err
}

func (c *Conn) Password(_ string) (string, error) {
	return c.Pass, nil
}

func (c *Conn) Disconnect() error {
	if c.conn != nil {
		return c.conn.Close()
	} else {
		return nil
	}
}
