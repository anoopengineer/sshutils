package sshutils

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
)

func (c *Conn) CopyToServer(localFile string, remoteDir string) error {
	f, err := os.Open(localFile)
	if err != nil {
		return err
	}
	defer f.Close()
	stat, err := f.Stat()
	fileName := stat.Name()
	mode := stat.Mode().Perm()
	size := stat.Size()
	base := path.Base(remoteDir)
	remotePath := remoteDir + "/" + fileName
	fmt.Println(remotePath)

	session, err := c.conn.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()

	in, _ := session.StdinPipe()
	defer in.Close()
	out, _ := session.StdoutPipe()

	cmd := "/usr/bin/scp -t -- " + remotePath

	if err := session.Start(cmd); err != nil {
		fmt.Println("Can't run " + err.Error())
		return err
	}
	fmt.Println("Process ran")

	in.Write([]byte(fmt.Sprintf("C%04o %d %s\n", mode, size, base)))
	fmt.Println(out.Read(make([]byte, 1, 1)))

	io.Copy(in, f)
	in.Close()
	fmt.Println("Everything went through")

	return session.Wait()
}

func (c *Conn) CopyFromServer(localDir string, remoteFile string) error {
	session, err := c.conn.NewSession()
	if err != nil {
		fmt.Println("In CopyFromServer. Unable to create a new session")
		return err
	}
	defer session.Close()
	in, _ := session.StdinPipe()
	out, _ := session.StdoutPipe()

	cmd := "/usr/bin/scp -f -- " + remoteFile
	if err := session.Start(cmd); err != nil {
		fmt.Println("In CopyFromServer. Unable to start command execution")
		return err
	}
	in.Write([]byte{0})

	var (
		mode int
		size int64
		name string
		file *os.File
	)

	reader := bufio.NewReader(out)
	for {
		line, err := reader.ReadString('\n')
		in.Write([]byte{0})
		if err != nil {
			break
		}
		if strings.HasPrefix(line, "C") {
			_, err := fmt.Sscanf(line, "C%04o %d %s\n", &mode, &size, &name)
			if err != nil {
				break
			}
			if file, err = os.Create(localDir + string(os.PathSeparator) + name); err != nil {
				fmt.Println("In CopyFromServer. Unable to create file ", name)
				return err
			}
			if _, err := io.CopyN(file, reader, size); err != nil {
				fmt.Println("In CopyFromServer. Unable to do io copy")
				return err
			}
			file.Close()
			in.Write([]byte{0})
		}
	}

	//cleanup
	in.Close()
	return session.Wait()
}
