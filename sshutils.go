package sshutils

func (c *Conn) Execute(cmd string) (string, error) {
	session, err := c.conn.NewSession()
	if err != nil {
		return "", err
	}
	defer session.Close()
	arr, err := session.CombinedOutput(cmd)
	return string(arr), err
}
